# freeCodeCamp Project: Survey Form

A hypothetical survey about mental health. Submission for the project "Survey Form", part of the [Responsive Web Design certification on freeCodeCamp](https://www.freecodecamp.org/learn/2022/responsive-web-design/). 

[View on Gitlab Pages](https://caesiumtea.gitlab.io/survey/). HTML and CSS only, so the form isn't actually configured to *do* anything or send anywhere.

Any feedback would be appreciated!

## License
The Hippocratic License is an *almost* open license that says you can do basically anything you want with this code as long as it doesn't hurt people. Check out [LICENSE.md](LICENSE.md) as well as the [Hippocratic License website](https://firstdonoharm.dev/).

## Acknowledgements
- Background photo from Unsplash user [Kelly Sikkema](https://unsplash.com/@kellysikkema?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
- Color palette generated with help from [Coolors.co](http://coolors.co)

## Author
Hey, I'm **caesiumtea**, AKA Vance! Feel free to contact me with any feedback.
- [Website and social links](https://caesiumtea.glitch.me/)
- [@caesiumtea_dev on Twitter](https://www.twitter.com/caesiumtea_dev)
- [@entropy@mastodon.social](https://mastodon.social/@entropy)
